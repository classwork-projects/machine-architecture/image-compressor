# Image Compressor: Integer and Logical Operations

This project was developed to study machine arithmetic by developing an image compressor and decompressor, including writing:
  - Linear bijections: a discrete cosine transform and a bijection
between RGB and component video (Y/PR/PB) color spaces.
  - Functions to put a signed or unsigned small integer into a word or extract a small integer from a word.
  - A lossy image compressor that takes a PPM image and compresses
the image by transforming color spaces and discarding information that is
not easily seen by the human eye.

**Usage:**
```
image -d [filename]  
image -c [filename]
```

## Architecture
The overall architecture is composed of two main modules; compression and decompression, which rely on four other modules:

1.  Image: Opens a ppm file and runs the compress or decompress functions from the compress interface

2.  Compress: This module is responsible for handling reading and writing images in either compressed or decompressed form.

3.  Blockpack: Packs 2x2 blocks into codewords, or unpacks a codeword into a 2x2 block.

4.  Bitpack: This module is used for packing and unpacking the codewords, which are a series of bits.
<br />

**Compression**

  - Reads in a image file (PPM filetype) from the command line or from standard input.

  - Trim the image with `void trim(pnm_ppm);` making the width and height dimensions even numbers if necessary.

  - Transform each pixel into component video color space from RGB color space. (convert to float).
    - `rgb_to_component()` is called on each pixel, converting RGB values to component video colors
      ```
      struct component_pixel {
      	float Y;
      	float Pb;
      	Float Pr;
      };
      ```

  - Pack each 2x2 block into a 32-bit word by taking the average value of the four pixels in the block for the chroma elements by calling `UArray_T compress` which returns an array of 64 bit words

  - Convert to four-bit values using `unsigned Arith_index_of_chroma(float x);` which returns a 4-bit quantized representation of the chroma value given a chroma value between -0.5 and +0.5.

  - Transform the four Y (luminance/luma) values of the pixels into cosine coefficients a,b,c, and d using DCT.

  - After all transformations, the compressed image is written to stdout
<br />

**Decompression**
  - First it reads the header of the compressed file and stores the width, height, and denominator values of the image. These values are used to create a 2D array to store the image's pixels. `Pnm_ppm new_image` allocates an array to store a ppm image

  - Next, the codewords are read (in sequence), and decompressed into their corresponding pixel values so they can be stored in the 2D array.

  - `UArray2_T decompress` stores the arrays that unpack returns into a two dimensional array

  - The values a, b, c, d, P<sub>B</sub> and P<sub>R</sub> are unpacked, and stored as local variables using `Bitpack_gets` from the *Bitpack* interface. `UArray_T unpack_word` unpacks 64 bit words and returns a 2x2 pixel block.

  - Subsequently, the 4-bit chroma codes are converted in `float Arith_chroma_of_index(unsigned n);`.`

  - The inverse of the DCT is taken to compute brightness' Y<sub>1</sub>, Y<sub>2</sub>, Y<sub>3</sub>, Y<sub>4</sub> from a, b, c, and d.

  - Next, these values are converted into RGB color values (range 0 to 255), and give us the corresponding 2x2 block using `void component_to_rgb` given a `component_pixel`, converts color values to RGB and stores them in a ppm

  - The values are put back into the appropriate spot in the 2D Array.

  - Lastly, these values are put into `pixmap→pixels`, and can output with Stdout with `ppmwrite(stdout, pixmap);`.
<br />

## Design
Considering which data needs to be accessed and seen by other modules was important to enhance program security. For example, some files utilize methods that other files do not need, as well as some data that should not be accessed. Following data encapsulation best practices was vital to the design.

**Blockpack**  
The private functions in `blockpack` are used to pack/unpack codewords to and from blocks. It has access to all of the methods in bitpack excluding those below.

`Force_smaller` forces large a, b, and c values to +0.3 or −0.3
before encoding and is utilized in the cosine transformation.

`cosine_transform` is utilized in performing the discrete cosine calculations.

`undo_cosine_transform` is used for calculating the inverse cosine values

**Bitpack**   
The non private functions in `bitpack` are reused in `blockpack`.

`shift_word` is private to `bitpack`. It makes sure
that the offset is within bounds, and applies the proper shift.

`bitpack_replace` is also private to `bitpack`, and is used for replacing a specified field within a word.

**Compress**  
The functions in `compress` are responsible for compression
and decompression of a given image. This file has access to the
functions from blockpack that aren't listed above.

`trim`ensures that the height and width are even, and trims
them if they are odd.

`rgb_to_component_apply`, given a PPM image pixel, the RGB values are converted to component video values. Private to compression.

`code_words_apply` packs a block of pixels into code_words
using a close.

`unpack_word_apply` converts codeword values to component video values.

`check_bounds` makes values 0.0 or 1.0 depending on which is closer.

`component_to_rgb_apply` converts component video values to RGB values for a given pixel.
