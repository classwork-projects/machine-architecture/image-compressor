#ifndef ARITH_BLOCKPACK_H
#define ARITH_BLOCKPACK_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "array.h"
#include "math.h"
#include "mem.h"
#include "assert.h"
#include "arith411.h"

#define BLOCKSIZE 2

typedef struct code_words {
    int counter;
    Array_T block; // component_pixel structs for a block
    Array_T code_words; // list of 32 bit code words
} *code_words;

typedef struct component_pixel {
    float Y;
    float Pb;
    float Pr;
} *component_pixel;

// given a 2x2 block of component_pixel structs, returns a 32 bit codeword
extern uint64_t pack_word(Array_T block);
// given a codeword, returns a 2x2 block of packed_block structs
extern Array_T unpack_word(uint64_t codeword);
// print packed image
extern void print_blocks(Array_T code_words, unsigned width, unsigned height);
// read packed blocks from file and return image as array
extern Array_T read_blocks(FILE *input, unsigned width, unsigned height);

#endif //ARITH_BLOCKPACK_H
