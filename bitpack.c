#include <stdio.h>
#include <stdbool.h>
#include "bitpack.h"
#include "except.h"
#include "assert.h"

Except_T Bitpack_Overflow = { "Overflow packing bits" };

const unsigned max_width = 64;

static inline uint64_t shift_word(uint64_t word, unsigned offset, bool right)
{
    assert(offset <= max_width);

    if (offset >= max_width)
    {
        word = 0;
    }
    else if (right)
    {
        word = (word >> offset);
    }
    else { // left shift
        word = (word << offset);
    }

    return word;
}


//check if unsigned n can fit into a given number (width) of bits
bool Bitpack_fitsu(uint64_t n, unsigned width){
     /* Check Bounds */
    assert(width <= max_width);

    uint64_t max = ~0;
    max = shift_word(max, width, false);
    max = ~max;

    if (n > max)
    {
        return false;
    }
    else
    {
        return true;
    }


}
//check if signed int can fit into a given width of bits
bool Bitpack_fitss( int64_t n, unsigned width){
     /* Check Bounds */
    assert(width <= max_width);

    int64_t min = ~0;
    min = (int64_t)(shift_word(min, width - 1, false));
    int64_t max = ~min;

    if (min <= n && n <= max)
    {
        return true;
    }
    else
    {
        return false;
    }
}

uint64_t Bitpack_getu(uint64_t word, unsigned width, unsigned lsb)
{
    assert(width <= max_width && lsb + width <= max_width);

    uint64_t temp = ~0;
    temp = shift_word(temp, (max_width - width), false);
    temp = shift_word(temp, (max_width - lsb - width), true);
    word = (word & temp);
    word = shift_word(word, lsb, true);

    return word;
}
int64_t Bitpack_gets(uint64_t word, unsigned width, unsigned lsb)
{
    assert(width <= max_width && lsb + width <= max_width);

    uint64_t temp = ~0;
    temp = shift_word(temp, (max_width - width), false);
    temp = shift_word(temp, (max_width - width - lsb), true);
    word = (word & temp);
    word = shift_word(word, (max_width - width - lsb), false);
    // arith right shift
    int64_t temp_word = word;
    unsigned int offset = max_width - width;
    if (offset <= max_width - 1) {
        temp_word = (temp_word >> offset);
    }
    else if (temp_word >= 0)
    {
        temp_word = 0;
    }
    else {
        temp_word = ~0;
    }

    return temp_word;
}


// replace the specified field within a word
static uint64_t Bitpack_replace(uint64_t word, unsigned width, unsigned lsb, int64_t value)
{
    int64_t temp = ~0;
    temp = shift_word(temp, (max_width - width), false);
    temp = shift_word(temp, (max_width - width - lsb), true);
    value = shift_word(value, lsb, false);
    value = (value & temp);
    word = (word & ~temp);
    word = (word | value);

    return word;
}


uint64_t Bitpack_newu(uint64_t word, unsigned width, unsigned lsb, uint64_t value)
{
    assert(width <= max_width && lsb + width <= max_width);


    if (Bitpack_fitsu(value, width))
    {
        word = Bitpack_replace(word, width, lsb, value);
    }
    else {
        RAISE(Bitpack_Overflow);
    }

    return word;
}


uint64_t Bitpack_news(uint64_t word, unsigned width, unsigned lsb,  int64_t value)
{
    assert(width <= max_width && lsb + width <= max_width);


    if (Bitpack_fitss(value, width))
    {
        word = Bitpack_replace(word, width, lsb, value);
    }
    else {
        RAISE(Bitpack_Overflow);
    }

    return word;
}

