#include "bitpack.h"
#include "blockpack.h"

#define A_COEFFICIENT 511
#define BCD_COEFFICIENT 50

#define A_LSB  23
#define B_LSB  18
#define C_LSB  13
#define D_LSB  8
#define Pb_LSB 4
#define Pr_LSB 0

#define A_WIDTH  9
#define B_WIDTH  5
#define C_WIDTH  5
#define D_WIDTH  5
#define Pb_WIDTH 4
#define Pr_WIDTH 4

typedef struct packed_block {
    // average brightness
    unsigned int a;
    /* degree to which the image gets brighter as we move from top to bottom, left to right, and the degree to which the pixels on one diagonal are brighter than the pixels on the other diagonal */
    signed int b, c, d;
    // chroma index
    unsigned int pba, pra;
} *packed_block;

typedef struct average {
    float pba;
    float pra;
} average;


/* PACKING */

// forces large a, b, and c values to +0.3 or −0.3 before encoding
static void force_smaller(float *bit)
{
    if (*bit > 0.3) {
        *bit = 0.3;
    }

    if (*bit < -0.3) {
        *bit = -0.3;
    }
}

// performs the necessary discrete cosine calculations
static packed_block cosine_transform(Array_T block, packed_block word)
{
    float Y1 = ((component_pixel)Array_get(block, 0))->Y;
    float Y2 = ((component_pixel)Array_get(block, 1))->Y;
    float Y3 = ((component_pixel)Array_get(block, 2))->Y;
    float Y4 = ((component_pixel)Array_get(block, 3))->Y;
    float a = ((Y4 + Y3 + Y2 + Y1) / 4.0);
    float b = ((Y4 + Y3 - Y2 - Y1) / 4.0);
    float c = ((Y4 - Y3 + Y2 - Y1) / 4.0);
    float d = ((Y4 - Y3 - Y2 + Y1) / 4.0);
    force_smaller(&b);
    force_smaller(&c);
    force_smaller(&d);

    word->a = (unsigned int)(roundf(a * A_COEFFICIENT));
    word->b = (signed int)(roundf(b * BCD_COEFFICIENT));
    word->c = (signed int)(roundf(c * BCD_COEFFICIENT));
    word->d = (signed int)(roundf(d * BCD_COEFFICIENT));

    return word;
}


// given a 2x2 block of component_pixel structs, returns a 32 bit codeword
uint64_t pack_word(Array_T block)
{
    // initialize packed block
    packed_block word = NEW(word);

        // get a 4 bit quantized representation of the chroma value of four given pixels into the packed_block struct
    average avg = {.pba = 0.0, .pra = 0.0};
            // find average
    for (int i = 0; i < Array_length(block); i++)
    {
        component_pixel p = (component_pixel)Array_get(block, i);
        (avg.pba) += p->Pb;
        (avg.pra) += p->Pr;
    }
    (avg.pba) /= (float)Array_length(block);
    (avg.pra) /= (float)Array_length(block);
    unsigned int chroma_pba = Arith_index_of_chroma(avg.pba);
    unsigned int chroma_pra = Arith_index_of_chroma(avg.pra);
    word->pba = chroma_pba;
    word->pra = chroma_pra;

    word = cosine_transform(block, word);

    // pack the block
    uint64_t code_word = 0;
    code_word = Bitpack_newu(code_word, A_WIDTH, A_LSB, word->a);
    code_word = Bitpack_news(code_word, B_WIDTH, B_LSB, word->b);
    code_word = Bitpack_news(code_word, C_WIDTH, C_LSB, word->c);
    code_word = Bitpack_news(code_word, D_WIDTH, D_LSB, word->d);
    code_word = Bitpack_newu(code_word, Pb_WIDTH, Pb_LSB, word->pba);
    code_word = Bitpack_newu(code_word, Pr_WIDTH, Pr_LSB, word->pra);

    FREE(word);

    return code_word;

}


/* UNPACKING */

// calculates inverse cosine values
static Array_T undo_cosine_transform(Array_T block, packed_block word)
{
    float a = ((float)(word->a) / A_COEFFICIENT);
    float b = ((float)(word->b) / BCD_COEFFICIENT);
    float c = ((float)(word->c) / BCD_COEFFICIENT);
    float d = ((float)(word->d) / BCD_COEFFICIENT);
    float Y1 = a - b - c + d;
    float Y2 = a - b + c - d;
    float Y3 = a + b - c - d;
    float Y4 = a + b + c + d;

    ((component_pixel)Array_get(block, 0))->Y = Y1;
    ((component_pixel)Array_get(block, 1))->Y = Y2;
    ((component_pixel)Array_get(block, 2))->Y = Y3;
    ((component_pixel)Array_get(block, 3))->Y = Y4;

    return block;
}

// given a codeword, returns a 2x2 block of packed_block structs
extern Array_T unpack_word(uint64_t code_word)
{
    // packed_block struct to unpack code word into
    packed_block word = NEW(word);
    word->pra = Bitpack_getu(code_word, Pr_WIDTH, Pr_LSB);
    word->pba = Bitpack_getu(code_word, Pb_WIDTH, Pb_LSB);
    word->d = Bitpack_gets(code_word, D_WIDTH, D_LSB);
    word->c = Bitpack_gets(code_word, C_WIDTH, C_LSB);
    word->b = Bitpack_gets(code_word, B_WIDTH, B_LSB);
    word->a = Bitpack_getu(code_word, A_WIDTH, A_LSB);

    // convert packed_block struct into array of component_pixel structs
    Array_T block = Array_new(BLOCKSIZE * BLOCKSIZE, sizeof(struct component_pixel));
    float Pb = Arith_chroma_of_index(word->pba);
    float Pr = Arith_chroma_of_index(word->pra);
    for (int i = 0; i < Array_length(block); i++)
    {
        component_pixel p = (component_pixel)Array_get(block, i);
        p->Pb = Pb;
        p->Pr = Pr;
    }

    block = undo_cosine_transform(block, word);

    FREE(word);

    return block;
}


/* PRINTING */

// print packed image
extern void print_blocks(Array_T code_words, unsigned width, unsigned height)
{
    printf("Compressed image format 2\n%u %u", width, height);
    printf("\n");

    for (int i = 0; i < Array_length(code_words); i++)
    {
        uint64_t code_word = *(uint64_t *)Array_get(code_words, i);
        for (int j = 3; j >= 0; j--)
        {
            char byte = (char)Bitpack_getu(code_word, 8, (j*8));
            putchar(byte);
        }
    }
}

// read packed blocks from file and return image as array
extern Array_T read_blocks(FILE *input, unsigned width, unsigned height)
{
    int length = (width * height) / (BLOCKSIZE * BLOCKSIZE);
    Array_T code_words = Array_new(length, sizeof(uint64_t));
    for (int i = 0; i < Array_length(code_words); i++)
    {
        uint64_t code_word = 0;
        for (int j = 3; j >= 0; j--)
        {
            uint64_t byte = getc(input);
            code_word = Bitpack_newu(code_word, 8, (j*8), byte);
        }
        *(uint64_t*)Array_get(code_words, i) = code_word;
    }

    return code_words;
}
