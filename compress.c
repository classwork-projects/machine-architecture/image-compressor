#include "a2methods.h"
#include "a2plain.h"
#include "compress.h"
#include "blockpack.h"
#include "uarray2.h"
#include "pnm.h"


typedef A2Methods_Array2 A2; // private abbreviation
A2Methods_T methods;

// given a PPM image, trims its height and width only if they are of odd values
static void trim(Pnm_ppm image)
{
    if (image->height % 2 != 0)
    {
        image->height -= 1;
    }

    if (image->width % 2 != 0)
    {
        image->width -= 1;
    }
}

// given a PPM image pixel, convert it's RGB values to component video values
static void rgb_to_component_apply(int col, int row, A2 unused, void *elem, void *cl)
{
    (void) unused;
    component_pixel c_pixel = malloc(sizeof(*c_pixel));
    const struct A2Methods_T *methods = ((Pnm_ppm)cl)->methods;
    Pnm_ppm img = (Pnm_ppm)cl;
    Pnm_rgb rgb_pixel = (Pnm_rgb)(methods->at(img->pixels, col, row));

    float R = (float)(rgb_pixel->red) / (float)(img->denominator);
    float G = (float)(rgb_pixel->green) / (float)(img->denominator);
    float B = (float)(rgb_pixel->blue) / (float)(img->denominator);

    float y = (0.299 * R) + (0.587 * G) + (0.114 * B);
    float pb = (-0.168736 * R) - (0.331264 * G) + (0.5 * B);
    float pr = (0.5 * R) - (0.418688 * G) - (0.081312 * B);

    c_pixel->Y = y;
    c_pixel->Pb = pb;
    c_pixel->Pr = pr;

    *(component_pixel)elem = (*c_pixel);
    FREE(c_pixel);
}

// given a block of pixels, packs it into code_words using a closure
static void code_words_apply(int col, int row, A2 unused, void *component_elem, void *codeword_cl)
{
    (void) unused;

    code_words words = (code_words)codeword_cl;

    int block_index = BLOCKSIZE * (col % BLOCKSIZE) + (row % BLOCKSIZE);

    *((component_pixel)Array_get(words->block, block_index)) = *(component_pixel)component_elem;
    (words->counter)++;

    if ((words->counter) % (BLOCKSIZE * BLOCKSIZE) == 0) {
        uint64_t code_word = pack_word(words->block);
        int word_index = (((words->counter) / (BLOCKSIZE * BLOCKSIZE)) - 1);
        *(uint64_t *)Array_get(words->code_words, word_index) = code_word;
    }
}

// given a PPM image, writes a compressed image to standard output
void compress(FILE *input)
{
    methods = array2_methods_plain;
    assert(methods);
    Pnm_ppm img = Pnm_ppmread(input, methods);
    trim(img);     // call trim on the image to ensure even width and height
    A2 component_img = methods->new(img->width, img->height, sizeof(struct component_pixel));
    methods->map_row_major(component_img, rgb_to_component_apply, img); // compress image

    // compress image array into code_words
    int block_len = BLOCKSIZE * BLOCKSIZE;
    int code_word_len = (img->width * img->height) / (block_len);

    struct code_words cl = {
            .counter = 0,
            .block = Array_new(block_len, sizeof(struct component_pixel)),
            .code_words = Array_new(code_word_len, sizeof(uint64_t))
    };

    img->methods->map_row_major(component_img, code_words_apply, &cl);
    Array_free(&(cl.block));
    Array_T code_words = cl.code_words;
    // print the compressed image to standard output using the original uncompressed image's dimensions
    print_blocks(code_words, img->width, img->height);

    methods->free(&component_img);
    Array_free(&code_words);
    Pnm_ppmfree(&img);
}


// given a codeword, convert values to component video values
static void unpack_word_apply(int col, int row, A2 unused, void *component_elem, void *codeword_cl)
{
    (void) unused;

    code_words words = (code_words)codeword_cl;
    if ((words->counter) % (BLOCKSIZE * BLOCKSIZE) == 0) {
        if (words->counter != 0) {
            Array_free(&(words->block));
        }
        int word_index = (words->counter) / 4;
        words->block = unpack_word(*(uint64_t *)Array_get(words->code_words, word_index));
    }

    int block_index = BLOCKSIZE * (col % BLOCKSIZE) + (row % BLOCKSIZE);
    *(component_pixel)component_elem = *(component_pixel)(Array_get(words->block, block_index));
    (words->counter)++;
}

static void check_bounds(float *value)
{
    if (*value < 0.0)
    {
        *value = 0.0;
    }

    if (*value > 1.0)
    {
        *value = 1.0;
    }
}

// given a PPM image pixel, convert it's component video values to RGB values
static void component_to_rgb_apply(int col, int row, A2 unused, void *elem, void *ppm_cl)
{
    (void) unused;
    Pnm_rgb rgb_pixel = malloc(sizeof(struct Pnm_rgb));
    const struct A2Methods_T *methods = ((Pnm_ppm)ppm_cl)->methods;
    Pnm_ppm img = (Pnm_ppm)ppm_cl;
    component_pixel c_pixel = (component_pixel)elem;

    float y  = c_pixel->Y;
    float pb = c_pixel->Pb;
    float pr = c_pixel->Pr;

    float R = (1.0 * y) + (0.0 * pb) + (1.402 * pr);
    float G = (1.0 * y) - (0.344136 * pb) - (0.714136 * pr);
    float B = (1.0 * y) + (1.772 * pb) + (0.0 * pr);

    // check RGB values are between 0 and 1
    check_bounds(&R);
    check_bounds(&G);
    check_bounds(&B);

    rgb_pixel->red   = (unsigned)(R * (img->denominator));
    rgb_pixel->green = (unsigned)(G * (img->denominator));
    rgb_pixel->blue  = (unsigned)(B * (img->denominator));

    (*(Pnm_rgb)methods->at(img->pixels, col, row)) = (*rgb_pixel);
    FREE(rgb_pixel);
}

// given a file of code words, unpacks them into 2x2 blocks of pixels and prints the decompressed image to standard output
void decompress(FILE *input)
{
    methods = array2_methods_plain;
    assert(methods);

    // create PPM image
    unsigned width, height;
    int value = fscanf(input, "Compressed image format 2\n%u %u", &width, &height);
    assert(value == 2);
    int newline = getc(input);
    assert(newline == '\n');
    struct Pnm_ppm img = {
            .width = width, .height = height,
            .denominator = 255, .pixels = NULL,
            .methods = methods
    };

    // decompress the code words
    Array_T code_words = read_blocks(input, img.width, img.height);
    A2 component_img = methods->new(img.width, img.height, sizeof(struct component_pixel));
    struct code_words cl = {
            .counter = 0,
            .block = NULL,
            .code_words = code_words};
    methods->map_row_major(component_img, unpack_word_apply, &cl);
    Array_free(&(cl.block));
    Array_free(&(cl.code_words));

    // convert component values to RGB
    img.pixels = methods->new(img.width, img.height, sizeof(struct Pnm_rgb));
    methods->map_row_major(component_img, component_to_rgb_apply, &img);

    Pnm_ppmwrite(stdout, &img);
    methods->free(&component_img);
    methods->free(&(img.pixels));
}
